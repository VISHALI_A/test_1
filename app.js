const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send(` <h1>Welcome to My Simple Homepage!</h1>
  <p>This is a basic Node.js application using Express.</p>`);
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
